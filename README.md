# Sobre

Este repositório contém os dados utilizados para a pesquisa que deu origem a monografia para o MBA em Tecnologia da Informação do Programa de Educação Continuada da Escola Politécnica da Universidade de São Paulo.

O projeto em questão realizou uma análise dos dados abertos relativos a lei Rouanet disponíveis em http://dados.cultura.gov.br/dataset/incentivos-da-lei-rouanet

Os arquivos CSV constantes deste repositório foram baixados da API do SALIC (Sistema de Apoio às Leis de Incentivo à Cultura) e é possível também acessar os arquivos do Jupiter Notebook (*(com a extensão **.ipynb**) e entender como foi possível chegar a alguns números e gráficos que constam da monografia.

Para executar os Notebooks Jupyter, é preciso ter Python 3, Pandas, Matplotlib e NumPy instalados e executar o `jupyter-notebook` no diretório raiz deste projeto.